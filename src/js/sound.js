import * as Tone from "tone";

let tone_started = false;
let synth;
//attach a click listener to a play button
document.querySelector("button")?.addEventListener("click", async () => {
  await Tone.start();
  tone_started = true;
  console.log("audio is ready");
  //create a synth and connect it to the main output (your speakers)
  synth = new Tone.MonoSynth({
    oscillator: {
      type: "sawtooth",
    },
    envelope: {
      attack: 0.1,
    },
  }).toDestination();
  synth.triggerAttackRelease("C4", "2n");
});

function degree53_to_freq(degree, root_freq) {
  return root_freq * Math.pow(2.0, degree / 53.0);
}

export function play_pitch(degree) {
  if (tone_started && synth) {
    const freq = degree53_to_freq(degree, 262.0);
    synth.triggerAttack(freq, "0.1", 1);
  }
}
export function stop_pitch() {
  if (tone_started && synth) {
    synth.triggerRelease();
  }
}
