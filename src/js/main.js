import * as Sound from "./sound";

// Working JS modulo function, as opposed to the % remainder operator
function mod(n, m) {
  return ((n % m) + m) % m;
}

let up0 = document.getElementById("up-0");
let up1 = document.getElementById("up-1");
let up2 = document.getElementById("up-2");
let down0 = document.getElementById("down-0");
let down1 = document.getElementById("down-1");
let down2 = document.getElementById("down-2");
let main_pitch = document.getElementById("main-pitch");
let current_degree = 0;
let current_accidental = null;
let current_notation_system = "sagittal_evo";
function change_current_degree(amount, up = true) {
  current_degree += amount;
  current_accidental = degreeToAccidental(current_degree, up);
  let accidental = "";
  if (current_notation_system == "saggittal_revo") {
    accidental = current_accidental.sagittalRevo;
  } else if (current_notation_system == "sagittal_evo") {
    accidental = current_accidental.sagittalEvo;
  }
  main_pitch.innerHTML =
    '<span class="smufl">' +
    "\ue014\ue050\ue014" +
    // String.fromCharCode(0xeb90 + current_accidental.staffPosition) +
    "\ueb9d" +
    accidental +
    " " +
    "\ueb9d" +
    // String.fromCharCode(0xeb90 + current_accidental.staffPosition) +
    "\ue0a4" +
    current_accidental.naturalName.toUpperCase() +
    "</span>" +
    " " +
    current_degree +
    "°53";
  console.log(current_accidental);
}
up0.addEventListener("click", () => {
  change_current_degree(1);
});

up1.addEventListener("click", () => {
  change_current_degree(5);
});

up2.addEventListener("click", () => {
  change_current_degree(31);
});

down0.addEventListener("click", () => {
  change_current_degree(-1, false);
});

down1.addEventListener("click", () => {
  change_current_degree(-5, false);
});

down2.addEventListener("click", () => {
  change_current_degree(-31, false);
});

main_pitch.addEventListener("mousedown", () => {
  Sound.play_pitch(current_degree);
});
main_pitch.addEventListener("mouseup", () => {
  Sound.stop_pitch();
});

const naturals = [0, 9, 18, 22, 31, 40, 49];
const naturalNames = ["c", "d", "e", "f", "g", "a", "b"];

function degreeToAccidental(degree, sharp = true) {
  let closestIndex = 0;
  let closestDeviation = 9999;
  for (let i = 0; i < naturals.length; i++) {
    let deviation;
    if (sharp) {
      deviation = mod(degree, 53) - naturals[i];
    } else {
      let deviation1 = mod(naturals[i] - mod(degree, 53), 53);
      let deviation2 = mod(naturals[i] - (mod(degree, 53) - 53), 53);
      deviation = Math.min(deviation1, deviation2);
      console.log("flat deviation: " + deviation);
    }
    if (deviation < closestDeviation && deviation >= 0) {
      closestIndex = i;
      closestDeviation = deviation;
    }
  }
  if (!sharp) {
    closestDeviation *= -1;
  }
  let sagittalRevo = deviationToSagittalRevo.get(closestDeviation);
  if (sagittalRevo == undefined) {
    sagittalRevo = "";
  }
  let sagittalEvo = deviationToSagittalEvo.get(closestDeviation);
  if (sagittalEvo == undefined) {
    sagittalEvo = "";
  }
  return {
    degree: degree,
    deivation: closestDeviation,
    natural: naturals[closestIndex],
    naturalName: naturalNames[closestIndex],
    staffPosition: closestIndex,
    sagittalRevo: sagittalRevo,
    sagittalEvo: sagittalEvo,
  };
}
let deviationToSagittalRevo = new Map();
deviationToSagittalRevo.set(1, "\ue302");
deviationToSagittalRevo.set(2, "\ue306");
deviationToSagittalRevo.set(3, "\ue310");
deviationToSagittalRevo.set(4, "\ue314");
deviationToSagittalRevo.set(5, "\ue318");
deviationToSagittalRevo.set(6, "\ue31e");
deviationToSagittalRevo.set(7, "\ue322");
deviationToSagittalRevo.set(8, "\ue32c");
deviationToSagittalRevo.set(9, "\ue330");
deviationToSagittalRevo.set(10, "\ue334");
deviationToSagittalRevo.set(-1, "\ue303");
deviationToSagittalRevo.set(-2, "\ue307");
deviationToSagittalRevo.set(-3, "\ue311");
deviationToSagittalRevo.set(-4, "\ue315");
deviationToSagittalRevo.set(-5, "\ue319");
deviationToSagittalRevo.set(-6, "\ue31f");
deviationToSagittalRevo.set(-7, "\ue323");
deviationToSagittalRevo.set(-8, "\ue32d");
deviationToSagittalRevo.set(-9, "\ue331");
deviationToSagittalRevo.set(-10, "\ue335");

let deviationToSagittalEvo = new Map();
deviationToSagittalEvo.set(1, "\ue302");
deviationToSagittalEvo.set(2, "\ue306");
deviationToSagittalEvo.set(3, "\ue307\ue262");
deviationToSagittalEvo.set(4, "\ue303\ue262");
deviationToSagittalEvo.set(5, "\ue262");
deviationToSagittalEvo.set(6, "\ue302\ue262");
deviationToSagittalEvo.set(7, "\ue306\ue262");
deviationToSagittalEvo.set(8, "\ue307\ue263");
deviationToSagittalEvo.set(9, "\ue303\ue263");
deviationToSagittalEvo.set(10, "\ue263");
deviationToSagittalEvo.set(-1, "\ue303");
deviationToSagittalEvo.set(-2, "\ue307");
deviationToSagittalEvo.set(-3, "\ue306\ue260");
deviationToSagittalEvo.set(-4, "\ue302\ue260");
deviationToSagittalEvo.set(-5, "\ue260");
deviationToSagittalEvo.set(-6, "\ue303\ue260");
deviationToSagittalEvo.set(-7, "\ue307\ue260");
deviationToSagittalEvo.set(-8, "\ue306\ue264");
deviationToSagittalEvo.set(-9, "\ue302\ue264");
deviationToSagittalEvo.set(-10, "\ue264");

function setButtonsToSystem(system = "sagittal_revo") {
  let up0 = document.getElementById("up-0");
  let up1 = document.getElementById("up-1");
  let up2 = document.getElementById("up-2");
  let down0 = document.getElementById("down-0");
  let down1 = document.getElementById("down-1");
  let down2 = document.getElementById("down-2");
  up0.innerHTML = getAccidentalButton(1, system);
  up1.innerHTML = getAccidentalButton(5, system);
  up2.innerHTML = getAccidentalButton(31, system);
  down0.innerHTML = getAccidentalButton(-1, system);
  down1.innerHTML = getAccidentalButton(-5, system);
  down2.innerHTML = getAccidentalButton(-31, system);
}

function getAccidentalButton(degreeChange = 1, system = "sagittal_revo") {
  let text = "";
  if (Math.abs(degreeChange) <= 10) {
    let accidental = "";
    if (system == "sagittal_revo") {
      accidental = deviationToSagittalRevo.get(degreeChange);
    } else if ((system = "sagittal_evo")) {
      accidental = deviationToSagittalEvo.get(degreeChange);
    }
    text += '<span class="smufl">' + accidental + "</span>";
  } else if (degreeChange == 31) {
    text += "5th";
  } else if (degreeChange == -31) {
    text += "-5th";
  }
  text += "<br/>" + degreeChange + '<span class = "small-degree">°53</span>';
  return text;
}

setButtonsToSystem("sagittal_evo");
change_current_degree(0);
