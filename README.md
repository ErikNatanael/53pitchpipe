# 53pitchpipe

Web interface to hear 53edo pitches

# 53 Pitch Pipe

## Description

Mobile web interface (PWA) for playing 53-EDO pitches as a guide when singing music in 53.

## Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage

## Support

## Contributing

## License

TODO For open source projects, say how it is licensed.
